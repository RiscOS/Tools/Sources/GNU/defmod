# Makefile for GNU defmod

COMPONENT  ?= defmod

CINCLUDES   = ${OSINC} -Isources -Isupport
CDEFINES    = -DPACE_BUILD -DTRACE=0
CFLAGS     += ${C_NOWARN_NON_ANSI_INCLUDES}
YACCFLAGS   = -y
OBJS        = asmhelp cheader chelp cstrong def defmod hdr \
              objasm realloc lookup x

VPATH = sources support

SOURCES_TO_SYMLINK  = $(wildcard sources/c/*)
SOURCES_TO_SYMLINK += $(wildcard sources/h/*)
SOURCES_TO_SYMLINK += $(wildcard sources/y/*)
SOURCES_TO_SYMLINK += $(wildcard support/c/*)
SOURCES_TO_SYMLINK += $(wildcard support/h/*)
SOURCES_TO_SYMLINK += $(wildcard support/s/*)

ifneq (Host,${APCS})
CDEFINES   += -DRISCOS -DEXECUTE_ON_RISCOS -DYYDEBUG=0 -DYYMAXDEPTH=0
OBJS       += callback resource riscos riscosa
LIBS        = ${OSLIB}
else
CDEFINES   += -DUNIX -DEXECUTE_ON_UNIX
OBJS       += unix
endif

include CApp

ifeq (,${MAKE_VERSION})

sources.c.defmod: sources.y.defmod
	${YACC} ${YACCFLAGS} -o $@ sources.y.defmod

o.defmod: sources.c.defmod
	${CC} ${CFLAGS} -o $@ sources.c.defmod

clean::
	${RM} sources.c.defmod

else

.y.c:
	${YACC} ${YACCFLAGS} -o $@ $<

endif

# Dynamic dependencies:
